﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHpIndicator : MonoBehaviour {
    public RectTransform hpContainer;
    private PlayerHP hp;

    private void Awake() {
        if (hp == null)
            hp = FindObjectOfType<PlayerHP>();

        if (hp != null)
            hp.onCurrrentHPWasChanged += getDmg;

        getDmg();
    }

    private void OnDestroy() {
        if (hp != null)
            hp.onCurrrentHPWasChanged -= getDmg;
    }

    private void getDmg() {
        for (int i = 0; i < hpContainer.childCount; i++) {
            if (i < hp.current) {
                hpContainer.GetChild(i).gameObject.SetActive(true);
            } else {
                hpContainer.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
}