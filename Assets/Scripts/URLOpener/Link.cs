﻿using UnityEngine;
using System.Runtime.InteropServices;

public class Link : MonoBehaviour {
    public string url = "https://play.google.com/store/apps/details?id=ru.serious07.bloodtample";

    public void OpenLinkJSPlugin() {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
            openWindow(url);
        else
            Application.OpenURL(url);
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
}