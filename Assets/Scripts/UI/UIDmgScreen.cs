﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDmgScreen : MonoBehaviour {
    public Image blood;

    private PlayerHP hp;
    private Color tmpColor;
    private IEnumerator cr_blood;

    private void Awake() {
        if (hp == null)
            hp = FindObjectOfType<PlayerHP>();

        if (hp != null)
            hp.onCurrrentHPWasChanged += OnGetDmg;
    }

    private void OnDestroy() {
        if (hp != null)
            hp.onCurrrentHPWasChanged -= OnGetDmg;
    }

    private void OnGetDmg() {
        if (cr_blood != null)
            StopCoroutine(cr_blood);

        cr_blood = cr_show_blood_screen();

        if (hp.current < hp.max)
            StartCoroutine(cr_blood);
    }

    private IEnumerator cr_show_blood_screen() {
        blood.color = new Color(blood.color.r, blood.color.g, blood.color.b, 0.7f);

        tmpColor.r = blood.color.r;
        tmpColor.g = blood.color.g;
        tmpColor.b = blood.color.b;
        tmpColor.a = blood.color.a;

        while (blood.color.a > 0.05f) {
            tmpColor.a = blood.color.a - Time.deltaTime;

            blood.color = tmpColor;
            yield return null;
        }

        cr_blood = null;
    }
}
