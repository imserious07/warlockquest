﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class PlayerHP : MonoBehaviour {
    public Action onCurrrentHPWasChanged;
    public ParticleSystem bloodParticles;

    private bool isDead = false;
    private PlayerControls playerControls;

    private Vector3 newSize;

    public int _current;
    public int current {
        get {
            return _current;
        }
        set {
            _current = value;

            if (_current < 0)
                _current = 0;

            if (_current > max)
                _current = max;

            if (onCurrrentHPWasChanged != null)
                onCurrrentHPWasChanged();

            if (_current != max && bloodParticles != null) {
                StartCoroutine(cr_show_blood());
            }

            if (!isDead && _current == 0) {
                StartCoroutine(OnDeath());
            }
        }
    }
    public int max;

    public void Awake() {
        current = max;

        if (playerControls == null)
            playerControls = GetComponent<PlayerControls>();
    }

    private IEnumerator cr_show_blood() {
        bloodParticles.Play();
        yield return new WaitForSeconds(1.5f);
        bloodParticles.Stop();
    }

    private IEnumerator OnDeath() {
        isDead = true;

        if (playerControls != null)
            playerControls.canMove = false;

        while (transform.localScale.x > 0.05f) {
            newSize.x = transform.localScale.x - Time.deltaTime;
            newSize.y = transform.localScale.y - Time.deltaTime;
            newSize.z = transform.localScale.z - Time.deltaTime;

            transform.localScale = newSize;

            yield return null;
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}