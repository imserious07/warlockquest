﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ControllerSoulsCounter : Singleton<ControllerSoulsCounter> {
    public int _collectedSouls;

    public Action aValueWasChanged;

    public int collectedSouls {
        get {
            return _collectedSouls;
        }

        set {
            _collectedSouls = value;

            if (aValueWasChanged != null)
                aValueWasChanged();

            if (_collectedSouls == needSoulsToOpenExit) {
                exit.isAllowEnter = true;
            }
        }
    }
    public int needSoulsToOpenExit;

    private RoomExit exit;

    public void Awake() {
        needSoulsToOpenExit = FindObjectsOfType<AIMovement>().Length;
        collectedSouls = 0;

        if (exit == null)
            exit = FindObjectOfType<RoomExit>();
    }
}