﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class UIRestartButton : MonoBehaviour {
    private Button restartButton;

    private void Awake() {
        restartButton = GetComponent<Button>();
        restartButton.onClick.AddListener(Restart);
    }

    private void Restart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}