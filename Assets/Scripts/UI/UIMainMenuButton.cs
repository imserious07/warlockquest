﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class UIMainMenuButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    public TextMeshProUGUI text;

    public void OnPointerEnter(PointerEventData eventData) {
        text.color = Color.white;
    }

    public void OnPointerExit(PointerEventData eventData) {
        text.color = Color.red;
    }
}