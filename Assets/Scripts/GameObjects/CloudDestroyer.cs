﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudDestroyer : MonoBehaviour {
    private void OnTriggerEnter2D(Collider2D collision) {
        print("Enter");

        if (collision.GetComponent<Cloud>()) {
            Destroy(collision.GetComponent<Cloud>().gameObject);
        }
    }
}