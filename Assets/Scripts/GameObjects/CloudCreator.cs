﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudCreator : MonoBehaviour {
    public Cloud cloudPrefub;

    public float minWaitTime = 15f;
    public float maxWaitTime = 40f;

    public float minSpawnPosY = -3;
    public float maxSpawnPosY = 3;

    private void Awake() {
        StartCoroutine(cr_create_cloud());
    }

    private IEnumerator cr_create_cloud() {
        while (true) {
            Instantiate(cloudPrefub.gameObject, transform.position + (Vector3.up * Random.Range(minSpawnPosY, maxSpawnPosY)), Quaternion.identity);

            yield return new WaitForSeconds(Random.Range(minWaitTime, maxWaitTime));
        }
    }
}