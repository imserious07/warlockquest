﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ControllerBuildLimit : Singleton<ControllerBuildLimit> {
    public int _currentBlocksAmount;

    public int currentBlocksAmount {
        set {
            _currentBlocksAmount = value;

            if (aValueWasChanged != null)
                aValueWasChanged();
        }
        get {
            return _currentBlocksAmount;
        }
    }
    public int maxBlocksAmount = 10;

    public Action aValueWasChanged;

    public bool isOverflow() {
        return currentBlocksAmount == maxBlocksAmount;
    }
}