﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerSuckOnEnemies : MonoBehaviour {
    public float activeRange = 5f;
    public float minDistanceToConsume = 0.6f;
    public List<ICanBeCollected> enemiesInProgress;
    public ParticleSystem particlesLeft;
    public ParticleSystem particlesRight;

    public string rightMouseButton;

    public Action aStartCust;
    public Action aStopCust;

    private ICanBeCollected collectble;
    private float step;
    private Collider2D[] targets;
    private List<int> idToRemove;
    private PlayerControls playerControls;

    private Vector3 newSize;

    private bool startCust = false;

    public void Awake() {
        enemiesInProgress = new List<ICanBeCollected>();
        playerControls = GetComponent<PlayerControls>();
    }

    public void Update() {
        if (Input.GetButtonDown(rightMouseButton) && !ControllerBuildLimit.Instance.isOverflow()) {
            startCust = true;

            if (playerControls.turnedLeft && particlesLeft != null) {
                particlesLeft.Play();
            }

            if (playerControls.turnedRight && particlesRight != null) {
                particlesRight.Play();
            }

            if (aStartCust != null)
                aStartCust();
        }

        if (Input.GetButtonUp(rightMouseButton) && startCust) {
            startCust = false;

            if (particlesLeft != null) {
                particlesLeft.Stop();
            }

            if (particlesRight != null) {
                particlesRight.Stop();
            }

            if (aStopCust != null) {
                aStopCust();
            }
        }

        if (Input.GetButton(rightMouseButton) && !ControllerBuildLimit.Instance.isOverflow()) {
            targets = Physics2D.OverlapCircleAll(transform.position, activeRange);

            foreach (Collider2D target in targets) {
                collectble = target.GetComponent<ICanBeCollected>();

                if (target.GetComponent<ICanBeCollected>() != null && !enemiesInProgress.Contains(collectble)) {
                    ControllerBuildLimit.Instance.currentBlocksAmount++;

                    ControllerSoulsCounter.Instance.collectedSouls++;

                    enemiesInProgress.Add(collectble);
                    collectble.startSucking();
                }

                collectble = null;
            }
        }

        idToRemove = new List<int>();

        // Calculate sucked object position
        for (int i = 0; i < enemiesInProgress.Count; i++) {
            step = enemiesInProgress[i].suckingSpeed * Time.deltaTime;

            enemiesInProgress[i].transf.position = Vector3.MoveTowards(enemiesInProgress[i].transf.position, transform.position, step);
            enemiesInProgress[i].transf.Rotate(Vector3.forward, enemiesInProgress[i].rotationSpeed);

            if (enemiesInProgress[i].transf.localScale.x > enemiesInProgress[i].minScaleSize) {
                newSize.x = enemiesInProgress[i].transf.localScale.x - 0.01f;
                newSize.y = enemiesInProgress[i].transf.localScale.y - 0.01f;
                newSize.z = enemiesInProgress[i].transf.localScale.z - 0.01f;

                enemiesInProgress[i].transf.localScale = newSize;
            }

            if (Vector3.Distance(enemiesInProgress[i].transf.position, transform.position) <= minDistanceToConsume) {
                idToRemove.Add(i);
            }
        }

        foreach (int id in idToRemove) {
            if (enemiesInProgress[id].aConsumed != null)
                enemiesInProgress[id].aConsumed();

            Destroy(enemiesInProgress[id].transf.gameObject);
            enemiesInProgress.RemoveAt(id);
        }

        idToRemove.Clear();

        //foreach (ICanBeCollected unit in enemiesInProgress) {
        //    step = unit.suckingSpeed * Time.deltaTime;

        //    unit.transf.position = Vector3.MoveTowards(unit.transf.position, transform.position, step);
        //    unit.transf.Rotate(Vector3.forward, 3f);

        //    if (Vector3.Distance(unit.transf.position, transform.position) <= minDistanceToConsume) {
        //        enemiesInProgress.Remove(unit);
        //        Destroy(unit.transf.gameObject);
        //    }
        //}
    }
}