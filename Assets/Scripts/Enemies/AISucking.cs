﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class AISucking : MonoBehaviour, ICanBeCollected {
    public AudioClip sound1;
    public AudioClip sound2;

    public bool _isCollected;
    public float _suckingSpeed;
    public Transform _transform;
    public Action _aStartBeenSucked;
    public Action _aConsumed;
    public float _rotationSpeed;
    public float _minScaleSize;

    public bool isCollected {
        get {
            return _isCollected;
        }

        set {
            _isCollected = value;
        }
    }

    public float suckingSpeed {
        set {
            _suckingSpeed = value;
        }
        get {
            return _suckingSpeed;
        }
    }

    public Transform transf {
        set {
            _transform = value;
        }
        get {
            if (_transform == null) {
                _transform = transform;
            }

            return _transform;
        }
    }

    public Action aStartBeenSucked {
        get {
            return _aStartBeenSucked;
        }

        set {
            _aStartBeenSucked = value;
        }
    }

    public Action aConsumed {
        get {
            return _aConsumed;
        }

        set {
            _aConsumed = value;
        }
    }

    public float rotationSpeed {
        get {
            return _rotationSpeed;
        }
        set {
            _rotationSpeed = value;
        }
    }

    public float minScaleSize {
        get {
            return _minScaleSize;
        }

        set {
            _minScaleSize = value;
        }
    }

    private Rigidbody2D rb2d;
    private Collider2D col2D;

    public void Awake() {
        rb2d = GetComponent<Rigidbody2D>();
        transf = GetComponent<Transform>();
        col2D = GetComponent<Collider2D>();
    }

    public void startSucking() {
        rb2d.isKinematic = true;
        rb2d.velocity = Vector2.zero;
        col2D.enabled = false;

        if (SoundsController.Instance != null &&
            sound1 != null &&
            sound2 != null) {
            int rnd = UnityEngine.Random.Range(0, 2);

            if (rnd == 0) {
                SoundsController.Instance.Play(sound1);
            } else {
                SoundsController.Instance.Play(sound2);
            }
        }

        if (aStartBeenSucked != null)
            aStartBeenSucked();
    }
}