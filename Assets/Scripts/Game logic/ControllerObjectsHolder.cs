﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ControllerObjectsHolder : Singleton<ControllerObjectsHolder> {
    public Grid grid;
    public Tilemap walls;

    public void Awake() {
        if (grid == null)
            grid = FindObjectOfType<Grid>();
    }
}