﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface ICanBeCollected {
    bool isCollected { set; get; }
    Transform transf { set; get; }
    float suckingSpeed { set; get; }
    float rotationSpeed { set; get; }
    float minScaleSize { set; get; }
    Action aStartBeenSucked { set; get; }
    Action aConsumed { set; get; }

    void startSucking();
}