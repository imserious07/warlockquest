﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class IntroText : MonoBehaviour {
    public List<string> textCollection;
    public TextMeshProUGUI text;
    public AudioSource audioSource;
    public AudioClip charSound;

    private string currentText = "";
    private int charsCount;
    private int currentStep = 0;
    private bool textUpdated = false;
    private IEnumerator activeCorutine;

    public string nextSceneName;

    public void Update() {
        if (Input.anyKeyDown && !textUpdated) {
            if(currentStep < textCollection.Count) {
                activeCorutine = cr_updateText();
                StartCoroutine(cr_updateText());
            } else {
                SceneManager.LoadScene(nextSceneName);
            }
        }
    }

    private IEnumerator cr_updateText() {
        textUpdated = true;

        charsCount = 0;

        while (currentText.Length != textCollection[currentStep].Length) {
            charsCount++;
            currentText = textCollection[currentStep].Substring(0, charsCount);
            text.text = currentText;

            audioSource.PlayOneShot(charSound);

            yield return new WaitForSeconds(0.05f);
        }

        textUpdated = false;

        currentStep++;
        activeCorutine = null;
    }
}