﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
    public Button playGame;
    public Button exitGame;

    public string introSceneName = "Intro";

    public void Awake() {
        playGame.onClick.AddListener(StartGame);

        if (Application.platform == RuntimePlatform.WebGLPlayer ||
            Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.LinuxEditor ||
            Application.platform == RuntimePlatform.OSXEditor) {
            exitGame.gameObject.SetActive(false);
        } else {
            exitGame.onClick.AddListener(ExitGame);
        }
    }

    public void StartGame() {
        SceneManager.LoadScene(introSceneName);
    }

    public void ExitGame() {
        Application.Quit();
    }
}