﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour {
    public float speed;
    public Vector2 velocity;
    public Rigidbody2D rb2d;

    public void Awake() {
        rb2d.velocity = velocity * speed;
    }
}