﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour {
    private PlayerHP hp;
    private PlayerControls player;

    public int dmgByHit = 3;

    public void OnCollisionEnter2D(Collision2D collision) {
        hp = collision.collider.GetComponent<PlayerHP>();


        if (hp != null) {
            player = hp.GetComponent<PlayerControls>();

            if (!player.onGround) {
                hp.current -= dmgByHit;
            }
        }
    }
}
