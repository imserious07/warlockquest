﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class AIMovement : MonoBehaviour {
    public SidePoints leftSide;
    public SidePoints rightSide;
    public SidePoints leftWall;
    public SidePoints rightWall;

    public float speed = 10f;

    public bool isLeftPit;
    public bool isRightPit;
    public bool isLeftWall;
    public bool isRightWall;

    public bool _canMove = true;

    public bool canMove {
        get {
            return _canMove;
        }

        set {
            _canMove = value;

            if (value == false) {
                rb2d.velocity = Vector2.zero;
                movmentDirection = Vector2.zero;
            }
        }
    }

    public Vector2 movmentDirection;
    private Vector2 directionBeforeStop;

    [HideInInspector]
    public Rigidbody2D rb2d;
    private AISucking aiSucking;

    private bool isSucked = false;
    private int hitsCounts;

    private void Awake() {
        rb2d = GetComponent<Rigidbody2D>();

        if (aiSucking == null)
            aiSucking = GetComponent<AISucking>();

        if (aiSucking != null) {
            aiSucking.aStartBeenSucked += StartBeenSucked;
        }

        if (Random.Range(0, 2) == 0) {
            movmentDirection = Vector2.left * speed;
        } else {
            movmentDirection = Vector2.right * speed;
        }
    }

    private void OnDestroy() {
        if (aiSucking != null) {
            aiSucking.aStartBeenSucked -= StartBeenSucked;
        }
    }

    private void StartBeenSucked() {
        isSucked = true;
    }

    private void FixedUpdate() {
        if (!isSucked) {
            RaycastHit2D[] hits = Physics2D.LinecastAll(leftSide.pos1.position, leftSide.pos2.position);
            hitsCounts = 0;

            foreach (RaycastHit2D hit in hits) {
                if (!hit.collider.isTrigger)
                    hitsCounts++;
            }

            if (hitsCounts == 0) {
                isLeftPit = true;
            } else {
                isLeftPit = false;
            }

            hits = Physics2D.LinecastAll(rightSide.pos1.position, rightSide.pos2.position);
            hitsCounts = 0;

            foreach (RaycastHit2D hit in hits) {
                if (!hit.collider.isTrigger)
                    hitsCounts++;
            }

            if (hitsCounts == 0) {
                isRightPit = true;
            } else {
                isRightPit = false;
            }

            hits = Physics2D.LinecastAll(leftWall.pos1.position, leftWall.pos2.position);
            hitsCounts = 0;

            foreach (RaycastHit2D hit in hits) {
                if (!hit.collider.isTrigger)
                    hitsCounts++;
            }

            if (hitsCounts > 1) {
                isLeftWall = true;
            } else {
                isLeftWall = false;
            }

            hits = Physics2D.LinecastAll(rightWall.pos1.position, rightWall.pos2.position);
            hitsCounts = 0;

            foreach (RaycastHit2D hit in hits) {
                if (!hit.collider.isTrigger)
                    hitsCounts++;
            }

            if (hitsCounts > 1) {
                isRightWall = true;
            } else {
                isRightWall = false;
            }

            rb2d.velocity = movmentDirection;
        }
    }

    // Update is called once per frame
    void Update () {
        if (!isSucked && canMove) {
            movmentDirection.y = rb2d.velocity.y;

            if (isLeftPit || isLeftWall) {
                movmentDirection.x = speed;
            } else if (isRightPit || isRightWall) {
                movmentDirection.x = -speed;
            }
        }
	}
}

[System.Serializable]
public struct SidePoints {
    public Transform pos1;
    public Transform pos2;
}