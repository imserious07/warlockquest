﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIBuildMaterialAmount : MonoBehaviour {
    public TextMeshProUGUI text;
    public Slider slider;

    private ControllerBuildLimit buildLimit;
    private Animator animator;

    public void Awake() {
        buildLimit = ControllerBuildLimit.Instance;
        animator = GetComponent<Animator>();

        if (buildLimit != null) {
            buildLimit.aValueWasChanged += ChangeSlider;
        }

        ChangeSlider();
    }

    public void OnDestroy() {
        if (buildLimit != null) {
            buildLimit.aValueWasChanged -= ChangeSlider;
        }
    }

    private void ChangeSlider() {
        if (animator != null) {
            animator.SetTrigger("valueWasChanged");
        }

        if (buildLimit != null) {
            slider.value = (float)buildLimit.currentBlocksAmount / buildLimit.maxBlocksAmount;
            text.text = buildLimit.currentBlocksAmount + "/" + buildLimit.maxBlocksAmount;
        }
    }

    private IEnumerator cr_ResetAnimation() {
        yield return new WaitForSeconds(1f);
        if (animator != null) {
            animator.ResetTrigger("valueWasChanged");
        }
    }
}