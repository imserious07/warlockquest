﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDmgSource : MonoBehaviour {
    private PlayerHP hp;

    public int dmgByHit = 1;

    public void OnCollisionEnter2D(Collision2D collision) {
        hp = collision.collider.GetComponent<PlayerHP>();

        if (hp != null) {
            hp.current -= dmgByHit;
        }
    }
}