﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicController : Singleton<MusicController> {
    public AudioSource source;

    public AudioClip menuMusic;
    public AudioClip gameMusic;

    private AudioClip currentMusic = null;

    public string currentScene;

    public void Update() {
        currentScene = SceneManager.GetActiveScene().name;

        if (currentMusic != menuMusic && currentScene == "MainMenu") {
            PlayMenuMusic();
        }

        if (currentMusic != gameMusic && currentScene == "Level 1") {
            PlayGameMusic();
        }

        if (currentMusic != null && currentScene == "Intro") {
            StopAllMusic();
        }
    }

    public void Awake() {
        if (Instance == this) {
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }

    public void PlayMenuMusic() {
        source.Stop();
        source.loop = true;
        currentMusic = menuMusic;
        source.clip = menuMusic;
        source.Play();
    }

    public void PlayGameMusic() {
        source.Stop();
        source.loop = true;
        currentMusic = gameMusic;
        source.clip = gameMusic;
        source.Play();
    }

    public void StopAllMusic() {
        currentMusic = null;
        source.loop = false;
        source.Stop();
    }
}