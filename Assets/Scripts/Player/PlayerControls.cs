﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerControls : MonoBehaviour {
    public Transform groundPos1;
    public Transform groundPos2;
    public Transform groundPos3;
    public Transform groundPos4;

    public float speed = 1f;
    public float jumpPower = 1f;

    public string horisontalAxies = "Horisontal";
    public string jumpButton = "Jump";

    public Action aStartMove;
    public Action aStopMove;
    public Action aStartFall;
    public Action aStopFall;

    public AudioClip jumpSound;

    public bool turnedLeft;
    public bool turnedRight;

    private IEnumerator cr_waitForAirState;

    public bool _canMove = true;
    public bool canMove {
        get {
            return _canMove;
        }

        set {
            _canMove = value;

            if (value == false) {
                rb2d.velocity = Vector2.zero;
                rb2d.isKinematic = true;
                movingDirection = Vector2.zero;
            } else {
                rb2d.isKinematic = false;
            }
        }
    }

    [HideInInspector]
    public Rigidbody2D rb2d;
    private Vector2 movingDirection;

    public bool onGround = true;

    private bool isMoving = false;
    private bool isFalling = false;

    private void Awake() {
        rb2d = GetComponent<Rigidbody2D>();
    }

    bool OnGround() {
        RaycastHit2D[] hits = Physics2D.LinecastAll(groundPos1.transform.position, groundPos2.transform.position);

        foreach (RaycastHit2D hit in hits) {
            if (hit.collider.CompareTag("Ground")) {
                return true;
            }
        }

        hits = Physics2D.LinecastAll(groundPos3.transform.position, groundPos4.transform.position);

        foreach (RaycastHit2D hit in hits) {
            if (hit.collider.CompareTag("Ground")) {
                return true;
            }
        }

        return false;
    }

    private IEnumerator cr_change_on_ground_state() {
        yield return new WaitForSeconds(0.5f);
        onGround = false;
        cr_waitForAirState = null;
    }

    // Update is called once per frame
    void Update () {
        if (canMove) {
            if (!OnGround() && onGround && cr_waitForAirState == null) {
                cr_waitForAirState = cr_change_on_ground_state();
                StartCoroutine(cr_waitForAirState);
            } else {
                onGround = OnGround();
            }

            // Jump
            if (onGround && Input.GetButtonDown(jumpButton)) {
                rb2d.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);

                if (SoundsController.Instance != null && jumpSound != null) {
                    SoundsController.Instance.Play(jumpSound);
                }
            }

            // Horisontal movement
            movingDirection.x = Input.GetAxis(horisontalAxies) * speed;
            movingDirection.y = rb2d.velocity.y;

            if(Input.GetAxis(horisontalAxies) == 0)
                movingDirection.x = 0;

            rb2d.velocity = movingDirection;

            if (rb2d.velocity.x < 0) {
                turnedLeft = true;
                turnedRight = false;
            } else if (rb2d.velocity.x > 0) {
                turnedLeft = false;
                turnedRight = true;
            }

            if (movingDirection.x != 0 && isMoving == false) {
                isMoving = true;
                if (aStartMove != null)
                    aStartMove();
            } else if (movingDirection.x == 0 && movingDirection.y == 0 && isMoving) {
                isMoving = false;
                if (aStartMove != null)
                    aStopMove();
            } else if (movingDirection.y < 0 && isFalling == false) {
                isFalling = true;
                if (aStartFall != null)
                    aStartFall();
            } else if (movingDirection.y >= 0 && isFalling) {
                isFalling = false;
                if (aStopFall != null)
                    aStopFall();
            }
        }
    }
}
