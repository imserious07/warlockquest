﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(PlayerControls), typeof(PlayerBuild))]
public class PlayerAnimations : MonoBehaviour {
    public Sprite Idle;
    public Sprite Run;
    public Sprite Fall;
    public Sprite Cust;

    private PlayerControls playerControls;
    private PlayerBuild playerBuild;
    private PlayerSuckOnEnemies playerSuckOnEnemies;
    private SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Awake() {
        playerControls = GetComponent<PlayerControls>();
        playerBuild = GetComponent<PlayerBuild>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        playerSuckOnEnemies = GetComponent<PlayerSuckOnEnemies>();

        playerControls.aStopMove += StopMove;
        playerControls.aStartMove += StartMove;
        playerControls.aStartFall += StartFall;
        playerControls.aStopFall += StopFall;
        playerBuild.aStartCust += StartCust;
        playerBuild.aStopCust += StopCust;
        playerSuckOnEnemies.aStartCust += StartCust;
        playerSuckOnEnemies.aStopCust += StopCust;
    }

    private void Update() {
        if (playerControls.rb2d.velocity.x < 0) {
            spriteRenderer.flipX = true;
        } else if (playerControls.rb2d.velocity.x > 0) {
            spriteRenderer.flipX = false;
        }
    }

    void StopMove() {
        spriteRenderer.sprite = Idle;
    }

    void StartMove() {
        spriteRenderer.sprite = Run;
    }

    void StartCust() {
        spriteRenderer.sprite = Cust;
    }

    void StopCust() {
        spriteRenderer.sprite = Idle;
    }

    void StartFall() {
        spriteRenderer.sprite = Fall;
    }

    void StopFall() {
        spriteRenderer.sprite = Idle;
    }
}
