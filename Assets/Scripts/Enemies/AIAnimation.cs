﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(AIMovement))]
public class AIAnimation : MonoBehaviour {
    private SpriteRenderer spriteRenderer;
    private AIMovement movement;

    // Use this for initialization
    void Awake () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        movement = GetComponent<AIMovement>();
    }
	
	// Update is called once per frame
	void Update () {
        if (movement.rb2d.velocity.x < 0) {
            spriteRenderer.flipX = true;
        } else if (movement.rb2d.velocity.x > 0) {
            spriteRenderer.flipX = false;
        }
	}
}
