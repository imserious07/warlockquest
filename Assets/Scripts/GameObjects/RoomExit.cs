﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomExit : MonoBehaviour {
    public string nextScene;
    public SpriteRenderer stoneSpiral;

    public bool _isAllowEnter = false;

    public bool isAllowEnter {
        set {
            _isAllowEnter = value;

            if (value) {
                StartCoroutine(cr_change_stone_color());
            }
        }
        get {
            return _isAllowEnter;
        }
    }

    private Vector3 newPlayerScale;
    private PlayerControls player;
    private bool isActive = false;

    private void Awake() {
        
    }

    private IEnumerator cr_wait_for_player(PlayerControls player) {
        player.canMove = false;

        foreach (AIMovement enemy in FindObjectsOfType<AIMovement>()) {
            enemy.canMove = false;
        }

        while (player.transform.localScale.x > 0.05f) {
            newPlayerScale.x = player.transform.localScale.x - Time.deltaTime;
            newPlayerScale.y = player.transform.localScale.y - Time.deltaTime;
            newPlayerScale.z = player.transform.localScale.z - Time.deltaTime;

            player.transform.localScale = newPlayerScale;
            yield return null;
        }

        SceneManager.LoadScene(nextScene);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (!isActive && isAllowEnter) {
            player = collision.GetComponent<PlayerControls>();

            if (player != null) {
                isActive = true;
                StartCoroutine(cr_wait_for_player(player));
            }
        }
    }

    private IEnumerator cr_change_stone_color() {
        while (true) {
            stoneSpiral.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

            yield return new WaitForSeconds(1f);
        }
    }
}