﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(SpriteRenderer))]
public class UIBridgeBlockHelp : MonoBehaviour {
    private Vector3 mousePos;
    private Vector3 newPos;
    private Vector3 offset = new Vector3(0.5f, 0.5f, 0f);
    private SpriteRenderer spriteRenderer;

    public Color normalColor;
    public Color alphaColor;

    private void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update () {
        if (ControllerBuildLimit.Instance.currentBlocksAmount > 0) {
            spriteRenderer.color = normalColor;
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            newPos = ControllerObjectsHolder.Instance.grid.WorldToCell(mousePos) + offset;

            transform.position = newPos;
        } else {
            spriteRenderer.color = alphaColor;
        }
	}
}
