﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BuildingBlockBullet : MonoBehaviour {
    public float rotationSpeed;
    public float speed;
    public Action<Vector3> aReachTargetPosition;
    public float distanceToFillBlock = 0.25f;

    private float step;
    private Vector3 newSize;
    private Vector3 targetPosition;

    public void Init(Vector3 minSize, Vector3 targetPos) {
        transform.localScale = minSize;

        targetPosition = targetPos;
    }

    public void Update() {
        if (targetPosition != null) {
            step = speed * Time.deltaTime;

            transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
            transform.Rotate(Vector3.forward, rotationSpeed);

            if (transform.localScale.x < 1f) {
                newSize.x = transform.localScale.x + 0.01f;
                newSize.y = transform.localScale.y + 0.01f;
                newSize.z = transform.localScale.z + 0.01f;

                transform.localScale = newSize;
            }

            if (Vector3.Distance(transform.position, targetPosition) <= distanceToFillBlock) {
                if (aReachTargetPosition != null)
                    aReachTargetPosition(targetPosition);

                Destroy(gameObject);
            }
        }
    }
}