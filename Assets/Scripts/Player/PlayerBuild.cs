﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System;

public class PlayerBuild : MonoBehaviour {
    public string leftMouseButton;
    public BuildingBlockBullet bulletPrefub;

    public Tile buildingTile;
    public Tile alphaBuildingTile;

    public Action aStartCust;
    public Action aStopCust;

    public AudioClip shootSound;

    public ParticleSystem particlesLeft;
    public ParticleSystem particlesRight;

    private List<Vector3Int> occupiedTilegridPositions = new List<Vector3Int>();
    private Vector3Int tilePosition;
    private Vector3 mousePos;
    private PlayerControls playerControls;

    private void Awake() {
        playerControls = GetComponent<PlayerControls>();
    }

    // Update is called once per frame
    private void Update () {
        if (Input.GetButtonDown(leftMouseButton)) {
            if (playerControls.turnedLeft && particlesLeft != null) {
                particlesLeft.Play();
            }

            if (playerControls.turnedRight && particlesRight != null) {
                particlesRight.Play();
            }

            if (aStartCust != null)
                aStartCust();
        }

        if (Input.GetButtonUp(leftMouseButton)) {
            if (particlesLeft != null) {
                particlesLeft.Stop();
            }

            if (particlesRight != null) {
                particlesRight.Stop();
            }

            if (aStopCust != null)
                aStopCust();
        }

        if (Input.GetButton(leftMouseButton) && ControllerBuildLimit.Instance.currentBlocksAmount > 0) {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            tilePosition = ControllerObjectsHolder.Instance.grid.WorldToCell(mousePos);

            if (ControllerObjectsHolder.Instance.walls.GetTile(tilePosition) == null &&
                !occupiedTilegridPositions.Contains(tilePosition)) {

                if (SoundsController.Instance != null && shootSound != null) {
                    SoundsController.Instance.Play(shootSound);
                }

                ControllerBuildLimit.Instance.currentBlocksAmount--;
                occupiedTilegridPositions.Add(tilePosition);

                ControllerObjectsHolder.Instance.walls.SetTile(tilePosition, alphaBuildingTile);

                BuildingBlockBullet bullet = Instantiate(bulletPrefub.gameObject, transform.position, Quaternion.identity).GetComponent<BuildingBlockBullet>();

                bullet.aReachTargetPosition += createBridgeBlock;
                bullet.Init(new Vector3(0.3f, 0.3f, 0.3f), mousePos);
            }
        }
	}

    private void createBridgeBlock(Vector3 pos) {
        tilePosition = ControllerObjectsHolder.Instance.grid.WorldToCell(pos);

        ControllerObjectsHolder.Instance.walls.SetTile(tilePosition, buildingTile);
    }
}
