﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UISouls : MonoBehaviour {
    public TextMeshProUGUI SoulsText;
    private Animator animator;

    public void Awake() {
        ControllerSoulsCounter.Instance.aValueWasChanged += onValueChanged;
        animator = GetComponent<Animator>();

        onValueChanged();
    }

    public void onValueChanged() {
        if (ControllerSoulsCounter.Instance.collectedSouls == ControllerSoulsCounter.Instance.needSoulsToOpenExit) {
            SoulsText.text = "Exit opened now!";
        } else {
            SoulsText.text = ControllerSoulsCounter.Instance.collectedSouls + "/" + ControllerSoulsCounter.Instance.needSoulsToOpenExit;
        }

        if (animator != null) {
            animator.SetTrigger("valueWasChanged");
            StartCoroutine(cr_backTrigger());
        }
    }

    private IEnumerator cr_backTrigger() {
        yield return new WaitForSeconds(1f);
        if (animator != null) {
            animator.ResetTrigger("valueWasChanged");
        }
    }
}