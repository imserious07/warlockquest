﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsController : Singleton<SoundsController> {
    public AudioSource soundsSource;

    void Awake () {
        if (Instance == this) {
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
	}

    public void Play(AudioClip clip) {
        soundsSource.PlayOneShot(clip);
    }
}
