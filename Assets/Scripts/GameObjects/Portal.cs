﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {
    private PlayerControls player;
    private AIMovement mob;
    private Vector3 newSize;
    private float currentSpeed;

    public Transform portalDestination;
    public Vector3 mobDirection;
    public float speed = 3f;

    private void OnTriggerEnter2D(Collider2D collision) {
        player = collision.GetComponent<PlayerControls>();
        mob = collision.GetComponent<AIMovement>();

        if (player != null) {
            StartCoroutine(cr_teleport_player(player));
        }

        if (mob != null) {
            StartCoroutine(cr_teleport_mob(mob));
        }
    }

    private IEnumerator cr_teleport_player(PlayerControls player) {
        player.canMove = false;

        currentSpeed = Time.deltaTime * speed;

        while (player != null && player.transform.localScale.x > 0.05f) {
            newSize.x = player.transform.localScale.x - currentSpeed;
            newSize.y = player.transform.localScale.y - currentSpeed;
            newSize.z = player.transform.localScale.z - currentSpeed;

            player.transform.localScale = newSize;

            yield return null;
        }

        player.transform.position = portalDestination.position;

        while (player != null && player.transform.localScale.x < 2f) {
            newSize.x = player.transform.localScale.x + currentSpeed;
            newSize.y = player.transform.localScale.y + currentSpeed;
            newSize.z = player.transform.localScale.z + currentSpeed;

            player.transform.localScale = newSize;

            yield return null;
        }

        player.canMove = true;
    }

    private IEnumerator cr_teleport_mob(AIMovement mob) {
        mob.canMove = false;

        currentSpeed = Time.deltaTime * speed;

        newSize.x = mob.transform.localScale.x;
        newSize.y = mob.transform.localScale.y;
        newSize.z = mob.transform.localScale.z;

        while (mob != null && newSize.x > 0.05f) {
            newSize.x -= currentSpeed;
            newSize.y -= currentSpeed;
            newSize.z -= currentSpeed;

            mob.transform.localScale = newSize;

            yield return null;
        }

        if (mob != null) {
            mob.transform.position = portalDestination.position;
        }

        while (mob != null && newSize.x < 1.5f) {
            newSize.x += currentSpeed;
            newSize.y += currentSpeed;
            newSize.z += currentSpeed;

            mob.transform.localScale = newSize;

            yield return null;
        }

        if (mob != null) {
            mob.canMove = true;
            mob.movmentDirection = mobDirection * mob.speed;
        }
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.green;

        Gizmos.DrawSphere(portalDestination.position, 0.3f);
    }
}